from .models import Dict
from rest_framework import serializers


class DictSerializer(serializers.ModelSerializer):

    class Meta:
        model = Dict
        fields = '__all__'



