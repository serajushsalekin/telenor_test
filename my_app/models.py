from django.db import models
from django.db.models.signals import pre_save


class Dict(models.Model):
    key = models.CharField(primary_key=True, unique=True, max_length=255, blank=True)
    value = models.TextField()

    def __str__(self):
        return self.key


def id_(sender, instance, *args, **kwargs):
    if not instance.key:
        qs = Dict.objects.count() + 1
        instance.key = f"{'key'}{qs}"


pre_save.connect(id_, sender=Dict)
