from threading import Timer
from rest_framework import viewsets
from .models import Dict
from .serializers import DictSerializer


def job():
    Dict.objects.all().delete()
    print('done')


class DictList(viewsets.ModelViewSet):
    queryset = Dict.objects.all()
    serializer_class = DictSerializer

    def get_queryset(self):
        queryset = Dict.objects.all()
        method = self.request.method
        try:
            q = self.request.GET.get('keys')
            if q:
                q_item = q.split(',')
                qs = queryset.filter(key__in=q_item)
                return qs
        except:
            pass
        if method == 'GET' or 'POST':
            t = Timer(300, job)
            t.start()
        return queryset






